﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Module1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Module1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        static List<Customer> _customers = new List<Customer>()
        {
            new Customer(){Id=0,Name="Sanket",Email="sanket@gmail.com",Phone="1111111111" },
            new Customer(){Id=1,Name="Sayali",Email="sayali@gmail.com",Phone="1111111111" }
        };

        [HttpGet]
        [Route("")]
        public IActionResult Get()
        {
            return Ok(_customers);
        }

        [HttpPost]
        [Route("")]
        public IActionResult Post([FromBody] Customer customer)
        {
            if (ModelState.IsValid)
            {
                _customers.Add(customer);
                return StatusCode(200, customer);
            }

            return BadRequest(ModelState);
        }
    }
}
