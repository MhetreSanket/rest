﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Module1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Module1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        static List<Product> _products = new List<Product>()
        {
            new Product(){ProductId=0,ProductName="Laptop",ProductPrice="200" },
            new Product(){ProductId=1,ProductName="SmartPhone",ProductPrice="100" }
        };

        [HttpGet]
        [Route("")]
        public IActionResult Get()
        {
            return Ok(_products);
        }

        [HttpPost]
        [Route("")]
        public IActionResult Post([FromBody] Product product)
        {
            _products.Add(product);
            return StatusCode(200, "Server says OK!");
        }

        [HttpPut]
        [Route("{id}")]
        public void Put(int id, [FromBody] Product product)
        {
            _products[id] = product;
        }

        [HttpDelete]
        [Route("{id}")]
        public void Delete(int id)
        {
            _products.RemoveAt(id);
        }
    }
}